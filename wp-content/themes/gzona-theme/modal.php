
<div class="modal fade" id="contactModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
<div class="modal-dialog" role="document">
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
<h4 class="modal-title" id="myModalLabel">Make Reservation</h4>
</div>
 
<div class="modal-body">
    <?php $formId = get_sub_field('contact_form_id'); ?>
      <?php $formTitle = get_sub_field('contact_form_name'); ?>
 <?php echo do_shortcode( '[contact-form-7 id="'.$formId.'" title="'.$formTitle.'"]' ); ?>
</div>
 
</div>
</div>
</div>