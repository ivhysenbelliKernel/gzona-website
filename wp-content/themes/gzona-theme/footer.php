	<?php
	/**
	 * The template for displaying the footer.
	 *
	 * Contains the closing of the #content div and all content after
	 *
	 * @package understrap
	 */

	// Exit if accessed directly.
	defined( 'ABSPATH' ) || exit;

	$container = get_theme_mod( 'understrap_container_type' );
	?>

	<?php get_template_part( 'sidebar-templates/sidebar', 'footerfull' ); ?>


	<div class="wrapper" id="wrapper-footer">

		<div class="container-1600 footer-1600">
			<div class="row">
				<div class="col-12">
					<div class="top-footer">
						<div class="row">
							<div class="col-md-8 m-auto">
								<div class="logo">
									<?php $logo = get_field('logo','options') ?>
									<img src="<?php echo $logo['url']; ?>" alt="">
								</div>
								<div class="address">
									<a href="http://maps.google.com/?q=Gzona+Restorant,
									+<?php the_field('footer_address','options'); ?>">
									<p class="text"><?php the_field('footer_address','options') ?></p>
								</a>
							</div>
							<div class="email">
								<?php $email = get_field_object('email_address','options'); ?>
								<a class="fancybox" href="#contact_form_pop"><?php echo $email['prepend']; ?><p><?php the_field('email_address','options'); ?></p></a>
								<div class="fancybox-hidden">
									<div id="contact_form_pop">
										<div class="form-data">
											<?php if(ICL_LANGUAGE_CODE=='en'): 
												$formId = get_sub_field('contact_form_id'); 
												$formTitle = get_sub_field('contact_form_name'); 
												echo do_shortcode( '[contact-form-7 id="1064" title="Contact Form"]' ); 
											else :
												$formId = get_sub_field('contact_form_id'); 
												$formTitle = get_sub_field('contact_form_name'); 
												echo do_shortcode( '[contact-form-7 id="1065" title="Contact Form"]' ); 
											endif; ?>

										</div>
									</div>
								</div>
							</div>

							<div class="phone">
								<?php if(ICL_LANGUAGE_CODE == 'sq'):
									$phone = get_field_object('phone_number','options');  ?>
									<a href="tel:<?php the_field('phone_number','options'); ?>" class="cta-phone"><?php echo $phone['prepend']; ?><p><?php the_field('phone_number','options'); ?></p></a>
									<?php 
								else:
									$phone = get_field_object('phone_number','options'); ?>
									<a href="tel:<?php the_field('phone_number','options'); ?>" class="cta-phone"><?php echo "BOOK NOW:"; ?><p>
										<?php the_field('phone_number','options'); 
										endif;?></p></a>
									</div>
									<div class="working-hours">
										<h2 class="footer-title"><?php the_field('working_hours_title','options'); ?></h2>
										<?php $working_hours_1 = get_field_object( 'working_hours_1','options'); ?>
										<p><span><?php echo $working_hours_1['prepend'] ?></span><?php echo $working_hours_1['value'] ?></p>
										<?php $working_hours_2 = get_field_object( 'working_hours_2','options'); ?>
										<p><span><?php echo $working_hours_2['prepend'] ?></span><?php echo $working_hours_2['value'] ?></p>
										<?php $working_hours_3 = get_field_object( 'working_hours_3','options'); ?>
										<p><span><?php echo $working_hours_3['prepend'] ?></span><?php echo $working_hours_3['value'] ?></p>
									</div>
									<div class="social-links">
										<?php while (have_rows('social_links','options')) : the_row(); ?>
											<a href="<?php the_sub_field('url') ?>"> <i class="fa fa-<?php the_sub_field('social_icon') ?>"></i></a>
										<?php endwhile; ?>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-12">
						<div class="bottom-footer">
							<div class="row">
								<div class="col-xl-6 copyright">
									<p><?php the_field('copyright_text','options') ?></p>
								</div>	
								<div class="col-xl-6">
									<?php if ( is_front_page() || is_home() ) :
									wp_nav_menu( array( 'theme_location' => 'footer') );

								else:
									wp_nav_menu( array( 'theme_location' => 'secondary_footer' ) );

								endif; ?>

							</div>
							
						</div>
					</div>
				</div>

			</div><!-- container end -->

		</div><!-- wrapper end -->

	</div><!-- #page we need this extra closing tag here -->
	<?php get_template_part('modal'); ?>
	<?php wp_footer(); ?>




</body>

</html>
