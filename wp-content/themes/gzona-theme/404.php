<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @package understrap
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

get_header();

$container = get_theme_mod( 'understrap_container_type' );
?>

<div class="wrapper" id="error-404-wrapper">

	<div class="<?php echo esc_attr( $container ); ?>" id="content" tabindex="-1">

		<div class="row">

			<div class="col-md-12 content-area" id="primary">

				<main class="site-main" id="main">

					<section class="section error-404">
						<div class="row row-bg">
							<div class="col-12">
								<div class="bg-different">
									<div class="data text-center">
										<h1> 404 </h1>
										<h3> <?php the_field('heading','options');?></h3>
										<p> <?php the_field('subheading','options')?> </p>
									   <div class="buttons">
									   	<?php while( have_rows('buttons','options')) : the_row(); ?>
													<div class="button">
													<a class="cta-button cta-black" href="<?php the_sub_field('cta_link')?>" > <?php the_sub_field('cta_text')  ?> </a>
													</div>
											<?php endwhile; ?>				
									   </div>
									</div>
								</div>
							</div>
						</div>
			
					</section><!-- .error-404 -->

				</main><!-- #main -->

			</div><!-- #primary -->

		</div><!-- .row -->

	</div><!-- #content -->

</div><!-- #error-404-wrapper -->

<?php get_footer(); ?>
