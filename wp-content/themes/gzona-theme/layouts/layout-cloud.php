<?php
if( have_rows('layouts') ):
	// loop through the rows of data
	while ( have_rows('layouts') ) : the_row();
		
		if( get_row_layout() == 'section_homepage_banner' ):
			get_template_part( 'sections/section', 'homepage-banner' );
		endif;

		if( get_row_layout() == 'section_main_banner' ):
			get_template_part( 'sections/section', 'main-banner' );
		endif;

		if( get_row_layout() == 'section_testimonials' ):
			get_template_part( 'sections/section', 'testimonials' );
		endif;

		if( get_row_layout() == 'section_about_us' ):
			get_template_part( 'sections/section', 'about-us' );
		endif;

		if( get_row_layout() == 'section_gallery' ):
			get_template_part( 'sections/section', 'gallery' );
		endif;

		if( get_row_layout() == 'section_contact' ):
			get_template_part( 'sections/section', 'contact' );
		endif;
			
		if( get_row_layout() == 'section_booking_form' ):
			get_template_part( 'sections/section', 'booking-form' );
		endif;

		if( get_row_layout() == 'section_staff' ):
			get_template_part( 'sections/section', 'staff' );
		endif;		
		
		if( get_row_layout() == 'section_food_items' ):
			get_template_part( 'sections/section', 'food-items' );
		endif;

		if( get_row_layout() == 'section_drink_items' ):
			get_template_part( 'sections/section', 'drink-items' );
		 endif;

		 if( get_row_layout() == 'section_legend' ):
			get_template_part( 'sections/section', 'legend' );
		 endif;
		 if( get_row_layout() == 'section_no_price' ):
			get_template_part( 'sections/section', 'menu-items' );
		 endif;
		 if( get_row_layout() == 'section_repeater' ):
			get_template_part( 'sections/section', 'repeater' );
		 endif;
		  if( get_row_layout() == 'section_menu_banner' ):
			get_template_part( 'sections/section', 'menu_banner' );
		 endif;
		 if( get_row_layout() == 'section_buttons' ):
			get_template_part( 'sections/section', 'buttons' );
		 endif;
		 if( get_row_layout() == 'section_special_menu' ):
			get_template_part( 'sections/section', 'special-menu' );
		 endif;
      if( get_row_layout() == 'special_food_items_no_price' ):
			get_template_part( 'sections/section', 'special-menu' );
		 endif;		
		 
		 
	endwhile;
endif;