<section id="<?php the_sub_field('section_id'); ?>" class="section section-gallery">
	<div class="row">
		<div class="col-12">
			<div class="carousel slide gallery-carousel" id="data-carousel" data-ride="carousel">
					<div class="carousel-inner">
						<?php $count = 0; ?>
						<?php while(have_rows('gallery')): the_row(); ?>
							<?php $image = get_sub_field('image'); ?>
							<?php //var_dump($image); ?>
							
						<div class="carousel-item <?php if($count == 0){ echo "active"; } ?>" style = "background-image: url('<?php echo $image['url']; ?>');">
							<!-- <div class="overlay-black"> -->
							<h1 class="title white"><?php the_sub_field('title'); ?></h1>
							<img src="<?php echo $image['url']; ?>" alt="" class="img-responsive">
							 <!-- </div> -->
						</div>
						<?php $count++; ?>
						<?php endwhile; ?>
							<?php get_template_part( 'partials/partial', 'button'); ?>
						</div>
						</div>
					<div class="navigation">
					 <a class="carousel-control-prev" href="#data-carousel" role="button" data-slide="prev">
					    <span></span>
					  </a>
					  <a class="carousel-control-next" href="#data-carousel" role="button" data-slide="next">
					    <span></span>
					  </a>
					</div>
		</div>
	</div>
</section>

