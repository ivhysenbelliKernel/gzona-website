<?php if(get_sub_field('enabledisable_section') == true) : ?>
<section id="<?php the_sub_field('section_id'); ?>" class="section section-booking-form">
	<div class="row">
		<div class="col-10 col-lg-8 m-auto">
				<?php $booking_form = get_sub_field('booking_form'); ?>
				<?php echo do_shortcode( $booking_form); ?>
		</div>
	</div>
</section>
<?php endif; ?>
