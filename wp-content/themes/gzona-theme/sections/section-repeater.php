<?php if(get_sub_field('single_image_carousel') == "Multiple"): ?>
<section id="<?php the_sub_field('section_id'); ?>" class="section section-repeater <?php if ( is_front_page() || is_home() ) {echo "repeater-home"; } ?>">
	<div class="row">
		<div class="col-12">
			<div class="carousel slide gallery-carousel" id="gallery-carousel">
				<ol class="carousel-indicators ">
               <?php $index = 0; ?>
					<?php while(have_rows('repeater_multi')): the_row(); ?>
                      <li data-target="#gallery-carousel" class="<?php if($index == 0){ echo "active"; } ?>"></li>
                      <?php $index++; ?>
				<?php endwhile; ?>
             </ol>
					<div class="carousel-inner">
						<?php $count = 0; ?>
						<?php while(have_rows('repeater_multi')): the_row(); ?>
							<?php $image = get_sub_field('image'); ?>
							<?php //var_dump($image); ?>
							
						<div class="carousel-item <?php if($count == 0){ echo "active"; } ?>" style = "background-image: url('<?php echo $image['url']; ?>');">
							<div class="overlay-black 	<?php if ( !(is_front_page()) || !(is_home()) ) {echo "overlay-visible"; } ?>""> 
								<h3 class="title white"><?php the_sub_field('title'); ?></h3>		
								<p class="description"><?php the_sub_field('description'); ?></p>
								<img src="<?php echo $image['url']; ?>" alt="" class="img-responsive">
							 </div> 
						</div>

						<?php $count++; ?>
						<?php endwhile; ?>

							
						</div>
						</div>
					 <div class="navigation">
					 <a class="carousel-control-prev" href="#gallery-carousel" role="button" data-slide="prev">
					    <span></span>
					  </a>
					  <a class="carousel-control-next" href="#gallery-carousel" role="button" data-slide="next">
					    <span></span>
					  </a>
					</div> 
		</div>
	</div>
</section>
<?php elseif(get_sub_field('section_id') == "menu-items"): ?>
	<?php $bg_image = get_sub_field('background_image'); ?>
	<section id="<?php the_sub_field('section_id'); ?>" class="section section-repeater <?php if ( is_front_page() || is_home() ) {echo "repeater-home"; } ?>" style="background-image: url('<?php echo $bg_image['url']; ?>');">
	<div class="overlay-black 	<?php if ( !(is_front_page()) && !(is_home()) ) {echo "overlay-visible"; } ?>""></div> 
	<div class="row">
		<div class="col-12">
			<div class="carousel slide gallery-carousel" id="gallery-carousel">
	
					<div class="carousel-inner">
						<?php $count = 0; ?>
						<?php while(have_rows('repeater_single')): the_row(); ?>
						<div class="carousel-item <?php if($count == 0){ echo "active"; } ?>">
							<div class="data">
								<h3 class="title white"><?php the_sub_field('title'); ?></h3>		
								<p class="description"><?php the_sub_field('description'); ?></p>
								<img src="<?php echo $image['url']; ?>" alt="" class="img-responsive">
							</div>
						</div>
						<?php $count++; ?>
						<?php endwhile; ?>
						</div>
						</div>
						<div class="scroll-down"></div>
		</div>
	</div>
		<?php $val = get_sub_field('display_legend'); ?>
		<?php if( $val == TRUE): ?>
				<?php while (have_rows('legend_data')): the_row(); ?>
				<div class="legend-section">
					<?php while(have_rows('legend')): the_row(); ?>
					<div class="legend-item">	
						<div class="symbol" style="background-color: <?php the_sub_field('color_picker'); ?>">
						  <span class="span-symbol"> <?php the_sub_field('symbol') ?> </span> 
						</div>
						<div class="content" >
							<span class="span-content"><?php the_sub_field('content'); ?></span>
						</div>
					</div>
					<?php endwhile; ?>
				</div>
			<?php endwhile; ?>
		<?php endif; ?>
</section>

<?php else: ?>
	<?php $bg_image = get_sub_field('background_image'); ?>
	<section id="<?php the_sub_field('section_id'); ?>" class="section section-repeater <?php if ( is_front_page() || is_home() ) {echo "repeater-home"; } ?>" style="background-image: url('<?php echo $bg_image['url']; ?>');">
	<div class="row">
		<div class="col-12">
			<div class="carousel slide gallery-carousel" id="gallery-carousel">
					<ol class="carousel-indicators">
               <?php $index = 0; ?>
					<?php while(have_rows('repeater_single')): the_row(); ?>
                      <li data-target="#gallery-carousel" data-slide-to="<?= $index; ?>" class="<?php if($index == 0){ echo "active"; } ?>"></li>
                      <?php $index++; ?>
					<?php endwhile; ?>
	            </ol>
	
					<div class="carousel-inner">
						<?php $count = 0; ?>
						<?php while(have_rows('repeater_single')): the_row(); ?>
						<div class="carousel-item <?php if($count == 0){ echo "active"; } ?>">
							<div class="data">
								<h3 class="title white"><?php the_sub_field('title'); ?></h3>		
								<p class="description"><?php the_sub_field('description'); ?></p>
								<img src="<?php echo $image['url']; ?>" alt="" class="img-responsive">
							</div>
						</div>
						<?php $count++; ?>
						<?php endwhile; ?>
						</div>
						</div>
					 <div class="navigation">
					 <a class="carousel-control-prev" href="#gallery-carousel" role="button" data-slide="prev">
					    <span></span>
					  </a>
					  <a class="carousel-control-next" href="#gallery-carousel" role="button" data-slide="next">
					    <span></span>
					  </a>
					</div> 
		</div>
	</div>
</section>
<?php endif; ?>