<?php
/*
Template Name: Page of Posts
*/
get_header(); 
?>

<?php 
// the query
$wpb_all_query = new WP_Query(array('post_type'=>'post', 'post_status'=>'publish', 'posts_per_page'=>-1)); ?>

<?php if ( $wpb_all_query->have_posts() ) : ?>
   <div class="container" id="content">
    <div class="row">
        <div class="col-12">
            <section class="section section-blog">
                <div class="container-1600 d-flex justify-content-center"> 
                        <div class="section-title text-center">
                            <h1><?php the_title(); ?></h1>
                            <h2><?php the_content(); ?></h2>
                        </div>                  
                </div>
                <div class="container-1600 ">        
                        <?php $count = 0; ?>
                        <?php while ( $wpb_all_query->have_posts() ) : $wpb_all_query->the_post(); ?>               
                            <div class="col-12">
                                <a href="<?php the_permalink(); ?>">    
                                    <div class="blog">
                                        <?php $var = get_the_post_thumbnail_url(); ?>
                                        <div class="image" style="background-image: url(<?php echo $var; ?>);"></div>
                                        <div class="content">
                                            <h3 class="title-blog"><?php echo wp_trim_words(get_the_title(), 20); ?></h3>
                                            <?php echo wp_trim_words( get_the_content(), 40); ?>
                                            <div class="button">
                                                <?php if(ICL_LANGUAGE_CODE=='en'): ?>
                                                <a class="cta-button cta-blog" href="<?php the_permalink(); ?>">Read more</a>
                                                <?php else: ?>
                                                <a class="cta-button cta-blog" href="<?php the_permalink(); ?>">Lexo më shumë</a>
                                            <?php endif;?>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <?php $count++; ?>  
                        <?php endwhile; ?>
                    
                    <?php wp_reset_postdata(); ?>
                </div>
                <?php else : ?>
                    <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
                <?php endif; ?>
            </section>
        </div>
    </div>
</div>

<?php get_footer(); ?>

