<section id="<?php the_sub_field('section_id'); ?>" class="section section-social">
	<div class="row">
		<div class="col-10 col-lg-8 m-auto">
		<div class="data text-center">
				<h1 class="title deep-cerise "><?php the_sub_field('title');?></h1>
				<p class="sub-title "><?php the_sub_field('sub_title');?></p>
			</div>
		</div>
	</div>
</section>