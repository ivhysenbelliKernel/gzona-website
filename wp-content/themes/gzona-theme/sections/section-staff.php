<section id="<?php the_sub_field('section_id'); ?>" class="section staff-section">
	<div class="row">
		<div class="col-12">
			<div class="data">
				<h1 class="title white">
					<?php the_sub_field('section_title'); ?>
				</h1>
			</div>
		</div>
		<div class="col-12 m-auto">
			<?php if(have_rows('staff_list')): ?>
				<div class="container-1600">
					<div class="row">
				<?php while(have_rows('staff_list')): the_row(); ?>
						<div class="col single-staff" >
							<?php $image = get_sub_field('image') ?>
							<div class="image">
								<img src="<?php echo $image['url']; ?>" alt="" class="staff-photo">
							</div>
							<div class="data">
								<h2 class="name"><?php the_sub_field('name'); ?></h2>
								<p class="position"><?php the_sub_field('position'); ?></p>
								<p class="description"><?php the_sub_field('description') ?></p>
							</div>
						</div>
				<?php endwhile; ?>
					</div>
				</div>
			<?php endif; ?>
		</div>
	</div>
</section>