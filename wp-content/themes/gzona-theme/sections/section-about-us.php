<?php if( get_sub_field('section_id') != ("special-button" || "new-button")): ?>
<section id="<?php the_sub_field('section_id'); ?>" class="section section-about-us">
	<div class="row">
		<div class="col-12">
			<div class="data text-center">
				<h1 class="title"><?php the_sub_field('title'); ?></h1>
				<p><?php the_sub_field('description') ?></p>
			</div>

		</div>
	</div>
</section>
<?php elseif( get_sub_field('section_id') == "valentines"): ?>
<section  class="section section-about-us <?php the_sub_field('section_id'); ?>">
	<div class="row">
		<div class="col-12">
			<div class="data text-center">
				<h1 class="title"><?php the_sub_field('title'); ?></h1>
				<p><?php the_sub_field('description') ?></p>
			</div>

		</div>
	</div>
</section>

<?php else: ?>

<section id="<?php the_sub_field('section_id'); ?>" class="section section-about-us">
	<div class="row button-row">
		<div class="col-12  m-auto">
			<div class="data text-center">
				<?php get_template_part( 'partials/partial', 'button'); ?>
			</div>
		</div>
	</div>
</section>

<?php endif; ?>