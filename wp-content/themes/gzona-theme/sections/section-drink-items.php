
<div class="col-lg-6 drinks-list ">
	<section class="section section-drink-items">
			<div class="col-12 col-lg-8 m-auto">
				<?php while(have_rows('section_title')): the_row(); ?>
				<div class="description">
					<div class="main-data">
					  <div class="title">
							<h3 class="title-different black">
								 <?php the_sub_field('title'); ?>
							</h3>
						</div>
					</div>
					<div class="price">
				 	<span><?php the_sub_field('price');?></span>
					</div>
				 </div>
				<?php endwhile;?>

				<hr>
				<ul class="list-items">
					<?php while(have_rows('drink_items')): the_row(); ?>
						<li>
							<p>
							<span class="product-title"> <?php the_sub_field('title') ?>
							<?php $radio = get_sub_field_object('radio_button'); ?>
									<?php if(!empty($radio)): ?>
									<?php $value = $radio['value'];?>
									<span class="span-option option-<?php echo $value; ?>"> <?= $radio['choices'][$value]; ?></span>
									<span class="span-option option-val"><?php the_sub_field('value') ?></span>
								<?php endif; ?>
							</span> 
							</p>

							<span><?php the_sub_field('price'); ?></span>
						</li>
					<?php endwhile; ?>
				</ul>
			</h3>
			</div>
		</div>
	</section>
</div>
