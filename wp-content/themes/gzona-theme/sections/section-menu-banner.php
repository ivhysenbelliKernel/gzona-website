<section id="<?php the_sub_field('section_id'); ?>" class="section section-repeater">
	<div class="row">
		<div class="col-12">
			<div class="carousel slide gallery-carousel" id="gallery-carousel" data-ride="carousel">
				<ol class="carousel-indicators">
					<?php $index = 0; ?>
					<?php while(have_rows('menu_banner')): the_row(); ?>
						<li data-target="#myCarousel" data-slide-to="<?php echo $index ?>" class="<?php if($index == 0) {echo "active"; } ?>"></li>
						<?php $index++; ?>
					<?php endwhile; ?>
				</ol>
				<div class="carousel-inner">
					<?php $count = 0; ?>
					<?php while(have_rows('menu_banner')): the_row(); ?>
						<?php $image = get_sub_field('image'); ?>
						<?php //var_dump($image); ?>
						
						<div class="carousel-item <?php if($count == 0){ echo "active"; } ?>" style = "background-image: url('<?php echo $image['url']; ?>');">
							<div class="overlay-black"> 
								<h3 class="title white"><?php the_sub_field('title'); ?></h3>		
								<p class="description"><?php the_sub_field('description'); ?></p>
								<div class="legend">
									<?php while(have_rows('legend')): the_row(); ?>
										<div class="col-12 legend-item">
											
											<div class="symbol" style="background-color: <?php the_sub_field('color_picker'); ?>">
												<span class="span-symbol"> <?php the_sub_field('symbol') ?> </span> 
											</div>
											<div class="content" >
												<span class="span-content"><?php the_sub_field('content'); ?></span>
											</div>

										</div>
									<?php endwhile; ?>
								</div>
								<img src="<?php echo $image['url']; ?>" alt="" class="img-responsive">
							</div> 
						</div>
						<?php $count++; ?>
					<?php endwhile; ?>
					<?php get_template_part( 'partials/partial', 'button'); ?>
				</div>
			</div>
			<div class="navigation">
				<a class="carousel-control-prev" href="#gallery-carousel" role="button" data-slide="prev">
					<span></span>
				</a>
				<a class="carousel-control-next" href="#gallery-carousel" role="button" data-slide="next">
					<span></span>
				</a>
			</div> 
		</div>

	</div>
</section>
