
	<section class="section section-food-items">
	<div class="col-10 col-lg-8 m-auto">
		<div class="data text-center">
				<h1 class="title deep-cerise"><?php the_sub_field('section_title'); ?></h1>
				<p class="sub-title"><?php the_sub_field('section_description') ?></p>
			</div>
		</div>
				<div class="col-10 col-lg-8 m-auto">
				<hr>
				<ul class="food-items">
					<?php while(have_rows('food_items')): the_row(); ?>
						<li>
							<p>
							<span class="product-title"> <?php the_sub_field('title') ?>
							</span> 
							<span class="ml"> <?php the_sub_field('description')?></span>
							</p>
							<span class="price"><?php the_sub_field('price'); ?></span>
						</li>
					<?php endwhile; ?>
				</ul>
			</div>
	</section>


