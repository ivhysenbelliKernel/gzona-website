<section id="<?php the_sub_field('section_id'); ?>" class="section section-contact">
			<div class="container-1600">
				<div class="row m-auto">
					<div class="col-12">
						<div class="form-area">
							<div class="data">
								<h1 class="title title-different"><?php the_sub_field('section_title'); ?></h1>
								<a  href="tel:+355<?php the_sub_field('phone_number'); ?>"><p>TEL:</p> +355<?php the_sub_field('phone_number'); ?></a>
							</div>
							<div class="form-data">
							<?php $formId = get_sub_field('contact_form_id'); ?>
							<?php $formTitle = get_sub_field('contact_form_name'); ?>
							<?php  ?>
							<?php echo do_shortcode( '[contact-form-7 id="'.$formId.'" title="'.$formTitle.'"]' ); ?>
							</div>
						</div>
					</div>
				</div>
			</div>
</section>


