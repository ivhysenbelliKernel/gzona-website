<div class="col-lg-12 drinks-list">
<section class="section section-legend">
	
					 <div class="row">
					 	<div class="col-12 col-lg-10 m-auto">
					 	 <?php while(have_rows('legend')): the_row(); ?>
						<style>
							
						</style>
						<div class="col-12 legend-item">
						
								<div class="symbol" style="background-color: <?php the_sub_field('color_picker'); ?>">
								  <span class="span-symbol"> <?php the_sub_field('symbol') ?> </span> 
							   </div>
								<div class="content" >
								<span class="span-content"><?php the_sub_field('content'); ?></span>
								</div>

						</div>
						<?php endwhile; ?>
					</div>
</div>
</section>
</div>
