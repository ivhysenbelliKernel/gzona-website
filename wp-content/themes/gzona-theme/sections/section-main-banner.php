<?php $image = get_sub_field('image') ?>
<?php if( get_sub_field('section_id') == "about-menu"): ?>
<section id="<?php the_sub_field('section_id'); ?>" class="section main-banner" style="background-image: url('<?php echo $image['url']; ?>');">
	<div class="row">
       
			<div class="data">
				<h1 class="title black">
					<?php the_sub_field('title'); ?>
				</h1>
				<p class="sub-title black">
					<?php the_sub_field('sub_title') ?>
				</p>

			</div>
								
		</div>	
	</div>
</section>
<?php elseif ( get_sub_field('section_id') == "social") :?>
	<section id="<?php the_sub_field('section_id'); ?>" class="section main-banner" style="background-image: url('<?php echo $image['url']; ?>');">
		<div class="row">
		<div class="col-12">	
			<div class="data">
				<h1 class="title black">
					<?php the_sub_field('title'); ?>
				</h1>
				<p class="sub-title black">
					<?php the_sub_field('sub_title') ?>
				</p>

			</div>
								
		</div>
	</div>
</section>
<?php else: ?>
<section id="<?php the_sub_field('section_id'); ?>" class="section main-banner">
	  <div class = "row">
      <div class="col-12">
					<div class="button-about">
				<a class="cta-button cta-black disabled" href="<?php the_sub_field('cta_link')?>" > <?php the_sub_field('cta_text')  ?> </a>
			</div>
			<div class="data">
				<h1 class="title black">
					<?php the_sub_field('title'); ?>
				</h1>
				<p class="sub-title black">
					<?php the_sub_field('sub_title') ?>
				</p>

			</div>
								
		</div>	
	</div>
		</div>

</section>
<?php endif; ?>
