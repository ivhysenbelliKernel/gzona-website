<section id="<?php the_sub_field('section_id');?>" class="section section-testimonials">
	<div class="row testimonials-row">
		<div class="col-12">
			<div class="data text-center">
				<h1 class="title white"><?php the_sub_field('section_title'); ?></h1>
			</div>
			<div class="testimonials">
				<div class="carousel" id="testimonials-carousel" data-ride="carousel">
					<div class="carousel-inner">
						<?php $count = 0; ?>
						<?php while(have_rows('testimonials')): the_row(); ?>
							<div class="carousel-item <?php if($count == 0){ echo "active"; } ?>">
								<div class="row col-12 col-lg-12 m-auto">
									<div class="col-md-12 m-auto testimonial">
										<div class="description">
											<?php do_action( 'wprev_tripadvisor_plugin_action', 1 );?>
											<div class="testimonial-arrow"></div>
										</div>

									</div>

								</div>
							</div>
							<?php $count++; ?>
						<?php endwhile; ?>
					</div>
					<div class="navigation">
						<a class="carousel-control-prev" href="#testimonials-carousel" role="button" data-slide="prev">
							<span></span>
						</a>
						<a class="carousel-control-next" href="#testimonials-carousel" role="button" data-slide="next">
							<span></span>
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>