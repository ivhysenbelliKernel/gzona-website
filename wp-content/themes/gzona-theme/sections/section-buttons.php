<section class="section section-buttons">
	<div class="container-fluid">
			<div class="row">
			<?php while( have_rows('buttons')) : the_row(); ?>
				<div class='col-sm'>
					<div class="buttons">
				      <a class="cta-buttons cta-<?php the_sub_field('button_id')?>" href="<?php the_sub_field('cta_link')?>" > <?php the_sub_field('cta_text')  ?> </a>
					</div>
				</div>
			<?php endwhile;?>
		</div>
	</div>
</section>