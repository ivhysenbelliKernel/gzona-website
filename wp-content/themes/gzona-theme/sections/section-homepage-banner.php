<?php if(get_sub_field('video_or_image') == "Image" )  : ?>
	<?php $image = get_sub_field('image') ?>
	<section id="<?php the_sub_field('section_id'); ?>" class="section homepage-banner" style="background-image: url('<?php echo $image['url']; ?>');">
		<div class="row">
			<div class="overlay-black">
			<div class="col-12">
				<div class="data">
					<?php $logo = get_sub_field('logo') ?>
					<img src="<?php echo $logo['url']; ?>" alt="" class="logo">
					<h3 class="sub-title deep-cerise"><?php the_sub_field('sub_title'); ?></h3>
					<?php get_template_part( 'partials/partial', 'button'); ?>
				</div>
			</div>
		</div>
</div>
	</section>
<?php elseif(get_sub_field('video_or_image') == "Video" ): ?>
	<?php $video = get_sub_field('video'); ?>
	<section class="section homepage-banner">
		<video autoplay muted id="video-background">
			  <source src="<?php echo $video['url'] ?>" type="video/mp4">
			</video>
			<div class="overlay-black">
				<div class="row">
					<div class="col-12">
						<div class="data">
							<?php $logo = get_sub_field('logo') ?>
							<img src="<?php echo $logo['url']; ?>" alt="" class="logo">
							<h3 class="sub-title deep-cerise"><?php the_sub_field('sub_title'); ?></h3>
							<?php get_template_part( 'partials/partial', 'button'); ?>
						</div>
					</div>
				</div>
		</div>
	</section>
<?php endif;?>