<div class="special-menu">
	<div class="col-lg-12 drinks-list">
		<section class="section section-drink-items">
			<div class="col-12 col-lg-8 m-auto">
		<?php while(have_rows('special_special_menu')): the_row(); ?>

				<section class="section section-about-us">
					<div class="row">
						<div class="col-12">
							<div class="data text-center">
								<h1 class="title"><?php the_sub_field('title'); ?></h1>
								<p><?php the_sub_field('description') ?></p>
							</div>

						</div>
					</div>
				</section>
				<?php while(have_rows('special_food_items_no_price')): the_row(); ?>
					<div class="description">
						<div class="main-data">
							<div class="title">
								<h3 class="title-different black">
									<?php the_sub_field('title'); ?>
								</h3>
							</div>
						</div>
					</div>
				<ul class="list-items">
						<?php while(have_rows('menu_items')): the_row(); ?>
							<li>
								<p>
									<span class="product-title"> <?php the_sub_field('title') ?>
								</span> 
							</p>
						</li>
					<?php endwhile; ?>
			</ul>
		
	</div>
</div>
</section>
<?php endwhile;?>
</div>
</div>
